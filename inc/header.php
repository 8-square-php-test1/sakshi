<?php 
$URL ='http://localhost/cpo/';
$BASE_URL='http://localhost/cpo/app/';
?>


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>CPO-CRUD</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
      }
      .main {
        width: 50%;
        margin: 50px auto;
    }
    .container{
      padding-top: 20px;
    }
    table, th,td {
      background-color: #ffffff;
      border: 1px solid gray;
      width: 100%;
      padding: 8px 10px;
      margin: 3px 0;
      

    }
    table{
      border-collapse: collapse;
    }
    th{
      color: #696969;
      border: 1px solid gray;
    }
    input[type=text], input[type=number],input[type=submit]{
      width: 100%;
      padding: 8px 12px;
      margin: 5px 0;
      display: inline-block;
      border: 1px solid #ccc;
      border-radius: 4px;
      box-sizing: border-box;
    }

    @media (min-width: 768px) {
      .bd-placeholder-img-lg {
        font-size: 3.5rem;
      }
    }


    </style>
    <!-- Custom styles for this template -->
   <!--  <link href="starter-template.css" rel="stylesheet"> -->
  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-dark bg-primary">
  <a class="navbar-brand" href="<?php echo $URL;?>index.php">CPO-CRUD</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo $BASE_URL;?>customer/index.php">Customer <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo $BASE_URL;?>product/index.php">Product</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo $BASE_URL;?>order/index.php">Order</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo $BASE_URL;?>view/viewpage.php">View Orders</a>
      </li>
      
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>

<main role="main" class="container">

 

  <aside></aside>

</main><!-- /.container -->
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="/docs/4.2/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
      <script src="/docs/4.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-zDnhMsjVZfS3hiP7oCBRmfjkQC4fzxVxFhBx8Hkz2aZX8gEvA/jsP3eXRCvzTofP" crossorigin="anonymous"></script></body>
</html>
