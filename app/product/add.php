
<?php

$base = '../../inc/';
include $base .'header.php';

?>
<body>
	<div class="container">
		<form method="post" action="add.php" name="form1" ><br/>
            
		    <h4>Enter Products details</h4> <br/>
            
		        <table id="table">
                    <tr>
                        <td>
                            <label>Product Name</label> <input type="text" name="products_name">
		                    <label>Description</label> <input type="text" name="description">
		                    <label>Created Date</label> <input type="text" name="created_date">
                            <label>Modified Date</label> <input type="text" name="modified_date">
		                    
                            
                            <input type="submit" name="Submit" value="Add">
                        </td>
                    </tr>
                </table>
		</form>
	</div>
</body>

<?php 
include $base .'footer.php';
include_once $base ."classes/Crud.php";
include_once $base ."classes/Validation.php";
 
$crud = new Crud();
$validation = new Validation();
 
if(isset($_POST['Submit'])) {    
    $products_name = $crud->escape_string($_POST['products_name']);
    $description = $crud->escape_string($_POST['description']);
    $created_date = $crud->escape_string($_POST['created_date']);
    $modified_date = $crud->escape_string($_POST['modified_date']);
    
        
    $msg = $validation->check_empty($_POST, array('products_name', 'description', 'created_date','modified_date'));
    
    
    // checking empty fields
    if($msg != null) {
        echo $msg;        
        //link to the previous page
        echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
    } 

    else { 
    	//die('testing');
        // if all the fields are filled (not empty) 
            
        //insert data to database    
        $result = $crud->execute("INSERT INTO products (products_name,description,created_date,modified_date) VALUES ('$products_name','$description',
            '$created_date','$modified_date')");
        header('Location:index.php');
        //display success message
        //echo "<font color='green'>Data added successfully.";
        //echo "<br/><a href='index.php'>View Result</a>";
    }

    }
    ?>

