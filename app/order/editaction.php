<?php
// die(test);
$base = '../../inc/';
// including the database connection file
include_once $base ."classes/Crud.php";
include_once $base ."classes/Validation.php";
 
$crud = new Crud();
$validation = new Validation();
 
if(isset($_POST['update']))
{    
    $id = $crud->escape_string($_POST['id']);
    $customer_id = $crud->escape_string($_POST['customer_id']);
    $order_status = $crud->escape_string($_POST['order_status']);
    $order_date = $crud->escape_string($_POST['order_date']);
    
    
    $msg = $validation->check_empty($_POST, array('customer_id', 'order_status', 'order_date'));

      
    // checking empty fields
    if($msg) {
        echo $msg;        
        //link to the previous page
        echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
    } 
    else {    
        //updating the table
        $result = $crud->execute("UPDATE customer_orders SET customer_id='$customer_id',order_status='$order_status',order_date='$order_date' WHERE order_id=$id");
        // var_dump($result);
        //redirectig to the display page. In our case, it is index.php
        header("Location: index.php");
    }
}
?>