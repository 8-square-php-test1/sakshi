<?php

$base = '../../inc/';
//including the database connection file
include_once $base ."classes/Crud.php";
 
$crud = new Crud();
 
//fetching data in descending order (lastest entry first)
$query = "SELECT * FROM customer_orders ORDER BY order_id DESC";
$result = $crud->getData($query);
//echo '<pre>'; print_r($result); exit;
?>

<?php include $base .'header.php';?> 
<body>
	<div class="container">
		<h5>Search By:</h5>
	    <div class="row">

	    	<div class="col-lg-4">
	    		<!-- <form class="form-inline my-2 my-lg-0" action="customerlist.php" method="POST">
			      <input class="form-control mr-sm-2" type="text" placeholder="Username" aria-label="Search">
			      <button class="btn btn-secondary my-2 my-sm-0" type="submit" name="submit">Search</button>
		    	</form> -->
		    	<form method="POST" action="">
					<input type="text" name="q" placeholder="CustomerID">
					<input type="submit" name="search" value="Search">
					
				</form>
	    		
	    	</div>
	    	<!-- <div class="col-lg-5">
	    		<form class="form-inline my-2 my-lg-0">
			      <input class="form-control mr-sm-2" type="text" placeholder="Address" aria-label="Search">
			      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
			    </form>
			    <form method="POST" action="">
					<input type="text" name="q" placeholder="address">
					<input type="submit" name="search" value="Search">
				</form>
	    		
	    	</div> -->
	    	<div class="col-lg-4">
	    		<h5>Add Details of Orders:</h5><br>
	    		<button type="button" class="btn btn-primary" onClick="document.location.href='add.php'">ADD INFO</button>
	    		
	    	</div>

	    	<div class="col-lg-4">
	    		<h5>Update Orders of 1st February,2016</h5><br>
	    		<button type="button" class="btn btn-primary" onClick="document.location.href='update.php'">UPDATE INFO</button>
	    		
	    	</div>
	    </div>
		<hr>
		<h5>Cutomers list: </h5><br/>
		<table class="table-bordered">
			<tr>
				<td>Order Code</td>
		        <td>Customer ID</td>
		        <td>Order Status</td>
		        <td>Order Date</td>
		        
		        <td>Action</td>
		    </tr>

		    <?php 
		    foreach ($result as $key => $res) {
		    //while($res = mysqli_fetch_array($result)) {   while loop throws error      
		        echo "<tr>";
		        echo "<td>".$res['order_id']."</td>";
		        echo "<td>".$res['customer_id']."</td>";
		        echo "<td>".$res['order_status']."</td>";
		        echo "<td>".$res['order_date']."</td>";
		        
		        echo "<td>
				        <a href=\"edit.php?id=$res[order_id]\">Edit</a>:
				        <a href=\"delete.php?id=$res[order_id]\" onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a>
				     </td>";        
		    }
		    ?>
		</table>
	</div>
</body>

<?php include $base .'footer.php';?>