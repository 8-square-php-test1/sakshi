
<?php

$base = '../../inc/';
include $base .'header.php';

?>
<body>
	<div class="container">
		<form method="post" action="add.php" name="form1" ><br/>
            
		    <h4>Enter your details</h4> <br/>
            
		        <table id="table">
                    <tr>
                        <td>
                            <label>Customer ID</label> <input type="text" name="customer_id">
		                    <label>Order Status</label> <input type="text" name="order_status">
		                    <label>Order Date</label> <input type="text" name="order_date">
		                    
                            
                            <input type="submit" name="Submit" value="Add">
                        </td>
                    </tr>
                </table>
		</form>
	</div>
</body>

<?php 
include $base .'footer.php';
include_once $base ."classes/Crud.php";
include_once $base ."classes/Validation.php";
 
$crud = new Crud();
$validation = new Validation();
 
if(isset($_POST['Submit'])) {    
    $customer_id = $crud->escape_string($_POST['customer_id']);
    $order_status = $crud->escape_string($_POST['order_status']);
    $order_date = $crud->escape_string($_POST['order_date']);
    
        
    $msg = $validation->check_empty($_POST, array('customer_id', 'order_status', 'order_date'));
    
    
    // checking empty fields
    if($msg != null) {
        echo $msg;        
        //link to the previous page
        echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
    } 

    else { 
    	//die('testing');
        // if all the fields are filled (not empty) 
            
        //insert data to database    
        $result = $crud->execute("INSERT INTO customer_orders (customer_id,order_status,order_date) VALUES ('$customer_id','$order_status','$order_date')");
        header('Location:index.php');
        //display success message
        //echo "<font color='green'>Data added successfully.";
        //echo "<br/><a href='index.php'>View Result</a>";
    }

    }
    ?>

