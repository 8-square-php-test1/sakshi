<?php
// including the database connection file
$base = '../../inc/';
include_once $base ."classes/Crud.php";
 
$crud = new Crud();
 
//getting id from url
$id = $crud->escape_string($_GET['id']);
// var_dump($id);
 
//selecting data associated with this particular id
$result = $crud->getData("SELECT * FROM customer_orders WHERE order_id=$id");
 
foreach ($result as $res) {
    $customer_id = $res['customer_id'];
    $order_status = $res['order_status'];
    $order_date = $res['order_date'];
    
}
?>

<?php include $base .'header.php';?>
    <body>
        <div class="container">
            <h5>Update details:</h5>
            <form name="form1" method="post" action="editaction.php">
                <table border="0">
                    <td> 
                        <label>Customer ID</label> <input type="text" name="customer_id" value="<?php echo $customer_id;?>">
                        <label>Order Status</label><select class="form-control" type="text" name="order_status" value="<?php echo $order_status;?>">
                                <option>Paid</option>
                                <option>Unpaid</option>
                            </select>
                        <label>Order Date</label> <input type="text" name="order_date" value="<?php echo $order_date;?>">

                        <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">                    
                        <input type="submit" name="update" value="Update">
                    </td>
                </table>
            </form>
        </div>

    </body>
<?php include $base .'footer.php';?>