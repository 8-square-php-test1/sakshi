<?php
// including the database connection file
$base = '../../inc/';
include_once $base ."classes/Crud.php";
 
$crud = new Crud();
 
//getting id from url
$id = $crud->escape_string($_GET['id']);
// var_dump($id);
 
//selecting data associated with this particular id
$result = $crud->getData("SELECT * FROM customers WHERE id=$id");
 
foreach ($result as $res) {
    $name= $res['name'];
    $address = $res['address'];
    $created_date = $res['created_date'];
    $modified_date = $res['modified_date'];
    
}
?>

<?php include $base .'header.php';?>
    <body>
        <div class="container">
            <h5>Update details:</h5>
            <form name="form1" method="post" action="editaction.php">
                <table border="0">
                    <td> 
                        <label>Name</label> <input type="text" name="name" value="<?php echo $name;?>">
                        <label>Address</label> <input type="text" name="address" value="<?php echo $address;?>">
                        <label>Created Date</label> <input type="text" name="created_date" value="<?php echo $created_date;?>">
                        <label>Modified Date</label> <input type="text" name="modified_date" value="<?php echo $modified_date;?>">

                        <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">                    
                        <input type="submit" name="update" value="Update">
                    </td>
                </table>
            </form>
        </div>

    </body>
<?php include $base .'footer.php';?>