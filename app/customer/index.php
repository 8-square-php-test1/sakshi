<?php

$base = '../../inc/';
//including the database connection file
include_once $base ."classes/Crud.php";
 
$crud = new Crud();
 
//fetching data in descending order (lastest entry first)
$query = "SELECT * FROM customers ORDER BY id DESC";
$result = $crud->getData($query);
//echo '<pre>'; print_r($result); exit;
?>

<?php include $base .'header.php';?> 
<body>
	<div class="container">
		<h5>Search By:</h5>
	    <div class="row">

	    	<div class="col-lg-4">
	    		<!-- <form class="form-inline my-2 my-lg-0" action="customerlist.php" method="POST">
			      <input class="form-control mr-sm-2" type="text" placeholder="Username" aria-label="Search">
			      <button class="btn btn-secondary my-2 my-sm-0" type="submit" name="submit">Search</button>
		    	</form> -->
		    	<form method="POST" action="customersearch.php">
					<input type="text" name="q" placeholder="username">
					<input type="submit" name="search" value="Search">
				</form>
	    		
	    	</div>
	    	<div class="col-lg-4">
	    		<!-- <form class="form-inline my-2 my-lg-0">
			      <input class="form-control mr-sm-2" type="text" placeholder="Address" aria-label="Search">
			      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
			    </form> -->
			    <form method="POST" action="addresslist.php">
					<input type="text" name="q" placeholder="address">
					<input type="submit" name="search" value="Search">
				</form>
	    		
	    	</div>
	    	<div class="col-lg-4">
	    		<button type="button" class="btn btn-primary" onClick="document.location.href='add.php'">ADD INFO</button>
	    		
	    	</div>
	    </div>
		<hr>
		<h5>Cutomers list: </h5><br/>
		<table class="table-bordered">
			<tr>
				<td>Name</td>
		        <td>ID</td>
		        <td>Address</td>
		        <td>Created Date</td>
		        <td>Modified Date</td>
		        <td>Action</td>
		    </tr>

		    <?php 
		    foreach ($result as $key => $res) {
		    //while($res = mysqli_fetch_array($result)) {         
		        echo "<tr>";
		        echo "<td>".$res['name']."</td>";
		        echo "<td>".$res['id']."</td>";
		        
		        echo "<td>".$res['address']."</td>";
		        echo "<td>".$res['created_date']."</td>";
		        echo "<td>".$res['modified_date']."</td>";
		        echo "<td>
				        <a href=\"edit.php?id=$res[id]\">Edit</a>:
				        <a href=\"delete.php?id=$res[id]\" onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a>
				     </td>";        
		    }
		    ?>
		</table>
	</div>
</body>

<?php include $base .'footer.php';?>