<?php
$base = '../../inc/';
//including the database connection file
include_once $base ."classes/Crud.php";
 
$crud = new Crud();
 
//getting id of the data from url
$id = $crud->escape_string($_GET['id']);
 
//deleting the row from table
//$result = $crud->execute("DELETE FROM users WHERE id=$id");
$result = $crud->delete($id, 'customers');
 
if ($result) {
    //redirecting to the display page (index.php in our case)
    header("Location:index.php");
}
?>