<?php
// die(test);
$base = '../../inc/';
// including the database connection file
include_once $base ."classes/Crud.php";
include_once $base ."classes/Validation.php";
 
$crud = new Crud();
$validation = new Validation();
 
if(isset($_POST['update']))
{    
    $id = $crud->escape_string($_POST['id']);
    $name = $crud->escape_string($_POST['name']);
    $address = $crud->escape_string($_POST['address']);
    $created_date = $crud->escape_string($_POST['created_date']);
    $modified_date = $crud->escape_string($_POST['modified_date']);
    
    
    $msg = $validation->check_empty($_POST, array('name', 'address','created_date','modified_date'));

      
    // checking empty fields
    if($msg) {
        echo $msg;        
        //link to the previous page
        echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
    } 
    else {    
        //updating the table
        $result = $crud->execute("UPDATE customers SET name='$name',address='$address',created_date='$created_date' ,modified_date='$modified_date' WHERE id=$id");
        // var_dump($result);
        //redirectig to the display page. In our case, it is index.php
        header("Location: index.php");
    }
}
?>