<?php

$base = '../../inc/';
//including the database connection file
include_once $base ."classes/Crud.php";
 
$crud = new Crud();
 
//fetching data in descending order (lastest entry first)
$query = "select customers.name,customers.id,customers.address,customer_orders.order_id, customer_orders_products.product_id,products.products_name,customer_orders.order_status,customer_orders.order_date from ((( customers inner join customer_orders on customers.id=customer_orders.customer_id) inner join customer_orders_products on customer_orders_products.order_id=customer_orders.order_id)) inner join products on products.id=customer_orders_products.product_id;";
$result = $crud->getData($query);
//echo '<pre>'; print_r($result); exit;
?>

<?php include $base .'header.php';?> 
<body>
	<div class="container">
		<h5>Search By:</h5>
	    <div class="row">

	    	<div class="col-lg-4">
	    		<!-- <form class="form-inline my-2 my-lg-0" action="customerlist.php" method="POST">
			      <input class="form-control mr-sm-2" type="text" placeholder="Username" aria-label="Search">
			      <button class="btn btn-secondary my-2 my-sm-0" type="submit" name="submit">Search</button>
		    	</form> -->
		    	<form method="POST" action="customersearch.php">
					<input type="text" name="q" placeholder="username">
					<input type="submit" name="search" value="Search">
				</form>
	    		
	    	</div>
	    	<div class="col-lg-4">
	    		<!-- <form class="form-inline my-2 my-lg-0">
			      <input class="form-control mr-sm-2" type="text" placeholder="Address" aria-label="Search">
			      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
			    </form> -->
			    <form method="POST" action="addresslist.php">
					<input type="text" name="q" placeholder="address">
					<input type="submit" name="search" value="Search">
				</form>
	    		
	    	</div>
	    	<!-- <div class="col-lg-4">
	    		<button type="button" class="btn btn-primary" onClick="document.location.href='add.php'">ADD INFO</button>
	    		
	    	</div> -->
	    </div>
		<hr>
		<h5>Cutomers list: </h5><br/>
		<table class="table-bordered">
			<tr>
				<td>Name</td>
		        <td>CustomerID</td>
		        <td>Address</td>
		        <td>Product ID</td>
		        <td>Product Name</td>
		        <td>Order Status</td>
		        <td>Order Date</td>
		       <!--  
		        <td>Action</td> -->
		    </tr>

		    <?php 
		    foreach ($result as $key => $res) {
		    //while($res = mysqli_fetch_array($result)) {         
		        echo "<tr>";
		        echo "<td>".$res['name']."</td>";
		        echo "<td>".$res['id']."</td>";
		        
		        echo "<td>".$res['address']."</td>";
		        echo "<td>".$res['product_id']."</td>";
		        echo "<td>".$res['products_name']."</td>";
		        echo "<td>".$res['order_status']."</td>";
		        echo "<td>".$res['order_date']."</td>";
		       //  echo "<td>
				     //    <a href=\"edit.php?id=$res[id]\">Edit</a>:
				     //    <a href=\"delete.php?id=$res[id]\" onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a>
				     // </td>";        
		    }
		    ?>
		</table>
	</div>
</body>

<?php include $base .'footer.php';?>